import '@/assets/scss/app.scss';

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import ElementUI from 'element-ui';

import App from './App.vue';
import router from './router';

Vue.use(BootstrapVue);
Vue.use(ElementUI);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
