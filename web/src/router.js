import Vue from 'vue';
import Router from 'vue-router';
import Base from '@/Base.vue';
import Home from '@/views/Home.vue';
import Login from '@/views/Login.vue';
import Registrar from '@/views/Registrar.vue';
import PreProjetos from '@/views/pre-projetos/PreProjetos.vue';
import Projetos from '@/views/projetos/Projetos.vue';
import RecuperacaodeSenha from '@/views/RecuperacaodeSenha.vue';

Vue.use(Router);

export default new Router({
  routes: [
    { path: '/', redirect: '/Login' },
    {
      path: '/',
      component: Base,
      children: [
        {
          path: '/home',
          name: 'home',
          component: Home,
        },
        {
          path: '/pre-projetos',
          name: 'pre-projetos',
          component: PreProjetos,
        },
        {
          path: '/projetos',
          name: 'projetos',
          component: Projetos,
        }
      ],
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/Registrar',
      name: 'Registrar',
      component: Registrar,
    },
    {
      path: '/RecuperacaodeSenha',
      name: 'RecuperacaodeSenha',
      component: RecuperacaodeSenha,
    }
  ]
});
